locals {
  dmarc = "v=DMARC1;p=reject;rua=mailto:postmaster@${var.zone};ruf=mailto:admin@${var.zone};aspf=r;adkim=r;fo=1"
}
