variable "zone" {
  type = string
  description = "DNS zone name."
}

variable "security_key_record" {
  type = string
  description = "Security key record for email provider"
}

variable "security_key_value" {
  type = string
  description = "Security key value for email provider"
}

variable "mx" {
  type = list(object({
    mail_server = string
    priority = number
  }))
  description = "List of MX records with priority"
  default = [ {
    mail_server = "mxext1.mailbox.org."
    priority = 10
  },
  {
    mail_server = "mxext2.mailbox.org."
    priority = 10
  },
  {
    mail_server = "mxext3.mailbox.org."
    priority = 20
  } ]
}

variable "dkim" {
  type = list(map(string))
  description = "List of DKIM records"
  default = [ {
    name = "mbo0001._domainkey"
    content = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2K4PavXoNY8eGK2u61LIQlOHS8f5sWsCK5b+HMOfo0M+aNHwfqlVdzi/IwmYnuDKuXYuCllrgnxZ4fG4yVaux58v9grVsFHdzdjPlAQfp5rkiETYpCMZwgsmdseJ4CoZaosPHLjPumFE/Ua2WAQQljnunsM9TONM9L6KxrO9t5IISD1XtJb0bq1lVI/e72k3mnPd/q77qzhTDmwN4TSNJZN8sxzUJx9HNSMRRoEIHSDLTIJUK+Up8IeCx0B7CiOzG5w/cHyZ3AM5V8lkqBaTDK46AwTkTVGJf59QxUZArG3FEH5vy9HzDmy0tGG+053/x4RqkhqMg5/ClDm+lpZqWwIDAQAB"
  },
  {
    name = "mbo0002._domainkey"
    content = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqxEKIg2c48ecfmy/+rj35sBOhdfIYGNDCMeHy0b36DX6MNtS7zA/VDR2q5ubtHzraL5uUGas8kb/33wtrWFYxierLRXy12qj8ItdYCRugu9tXTByEED05WdBtRzJmrb8YBMfeK0E0K3wwoWfhIk/wzKbjMkbqYBOTYLlIcVGQWzOfN7/n3n+VChfu6sGFK3k2qrJNnw22iFy4C8Ks7j77+tCpm0PoUwA2hOdLrRw3ldx2E9PH0GVwIMJRgekY6cS7DrbHrj/AeGlwfwwCSi9T23mYvc79nVrh2+82ZqmkpZSTD2qq+ukOkyjdRuUPck6e2b+x141Nzd81dIZVfOEiwIDAQAB"
  } ]
}

variable "spf" {
  type = string
  description = "SPF value"
  default = "v=spf1 include:mailbox.org ~all"
}

variable "autoconfig" {
  type = string
  description = "Autoconfig TXT value"
  default = "mailbox.org."
}

variable "autodiscover_target" {
  type = string
  description = "Auto discover target"
  default = "mailbox.org"
}

variable "comment" {
  type = string
  description = "Comment to add to resources"
  default = "creation = terraform, repo = tf-infrastructure"
}