resource "cloudflare_record" "security_key" {
  zone_id = data.cloudflare_zone.this.id
  name    = var.security_key_record
  type    = "TXT"
  value   = var.security_key_value
  comment = var.comment
}

resource "cloudflare_record" "mx" {
  for_each = { for m in var.mx : m.mail_server => m }
  zone_id  = data.cloudflare_zone.this.id
  name     = "@"
  type     = "MX"
  value    = each.key
  priority = each.value.priority
  comment  = var.comment
}

resource "cloudflare_record" "dkim" {
  for_each = { for m in var.dkim : m.name => m }
  zone_id  = data.cloudflare_zone.this.id
  name     = each.key
  type     = "TXT"
  value    = each.value.content
  comment  = var.comment
}

resource "cloudflare_record" "spf" {
  zone_id = data.cloudflare_zone.this.id
  name    = "@"
  type    = "TXT"
  value   = var.spf
  comment = var.comment
}

resource "cloudflare_record" "dmarc" {
  zone_id = data.cloudflare_zone.this.id
  name    = "_dmarc"
  type    = "TXT"
  value   = local.dmarc
  comment = var.comment
}

resource "cloudflare_record" "autoconfig" {
  zone_id = data.cloudflare_zone.this.id
  name    = "autoconfig"
  type    = "CNAME"
  value   = var.autoconfig
  comment = var.comment
}

resource "cloudflare_record" "autodiscover" {
  zone_id = data.cloudflare_zone.this.id
  name    = "_autodiscover._tcp"
  type    = "SRV"
  data {
    service  = "_autodiscover"
    proto    = "_tcp"
    name     = var.zone
    priority = 0
    weight   = 0
    port     = 443
    target   = var.autodiscover_target
  }
  comment = var.comment
}