# Email

Sets up the DNS records for email. By default this sets up [mailbox.org](https://mailbox.org/en/).

## Required Variables 

| Variable            | Source                    | Category  | 
|-------------------- | ------------------------- | --------- |
| zone                | E.g. example.com          | terraform |
| security_key_record | Provided by mail provider | terraform |
| security_key_value  | Provided by mail provider | terraform |

