# Cloudflare

Cloudflare provides various network services. Two large ones being DNS and CDN.

## Access

Access can be passed by the `CLOUDFLARE_API_TOKEN` environment variable. See the required variables for more information.

## Required Variables 

| Variable             | Source                                                                                       | Category | 
|--------------------- | -------------------------------------------------------------------------------------------- | -------- |
| CLOUDFLARE_API_TOKEN | [Instructions](https://developers.cloudflare.com/fundamentals/api/get-started/create-token/) | env      |
