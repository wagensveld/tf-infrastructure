resource "cloudflare_zone" "this" {
  account_id = var.account_id
  zone       = var.zone
}

resource "cloudflare_bot_management" "this" {
  zone_id = cloudflare_zone.this.id
  fight_mode = true
  auto_update_model = true
  enable_js = true
}