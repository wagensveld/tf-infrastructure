variable "account_id" {
  type = string
  description = "Cloudflare Account ID"
}

variable "zone" {
  type = string
  description = "DNS zone name."
}