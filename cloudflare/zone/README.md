# Zone

This provides some initial set up for a Cloudflare zone. Domain registration and NS records still need to be done manually.

## Required Variables 

| Variable   | Source                 | Category  | 
|----------- | ---------------------- | --------- |
| account_id | Provided by Cloudflare | terraform |
| zone       | E.g. example.com       | terraform |


