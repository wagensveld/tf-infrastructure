terraform {
  cloud {
    hostname = "app.terraform.io"
    organization = "wagensveld"
    workspaces {
      tags = ["zone"]
    }
  }
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.15"
    }
  }
}
