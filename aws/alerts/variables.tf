variable "region" {
  type = string
  description = "Default AWS region"
  default = "us-east-1"
}

variable "currency" {
  type = string
  description = "Currency to display"
  default = "AUD"
}

variable "monthly_threshold" {
  type = string
  description = "Threshold for billing alert"
  default = "10"
}

variable "email_address" {
  type = string
  description = "Email address to receive alerts"
}