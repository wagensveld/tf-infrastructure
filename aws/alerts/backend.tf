terraform {
  cloud {
    hostname = "app.terraform.io"
    organization = "wagensveld"
    workspaces {
      tags = ["billing-alert"]
    }
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.18"
    }
  }
}
