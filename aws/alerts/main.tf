resource "aws_cloudwatch_metric_alarm" "billing" {
  alarm_name          = "billing-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "EstimatedCharges"
  namespace           = "AWS/Billing"
  period              = 21600
  statistic           = "Maximum"
  threshold           = var.monthly_threshold
  treat_missing_data  = "missing"
  dimensions = {
    Currency = var.currency
  }
}

resource "aws_sns_topic" "billing" {
  name = "billing-alarm-notification-${var.currency}"
}

resource "aws_sns_topic_subscription" "billing" {
  topic_arn = aws_sns_topic.billing.arn
  protocol = "email"
  endpoint = var.email_address
}