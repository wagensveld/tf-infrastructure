variable "region" {
  type = string
  description = "Default AWS region"
  default = "ap-southeast-2"
}

variable "domain" {
  type = string
  description = "Static site domain name"
}

variable "comment" {
  type = string
  description = "Comment to add to resources"
  default = "creation = terraform, repo = tf-infrastructure"
}