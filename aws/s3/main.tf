resource "aws_s3_bucket" "this" {
  bucket = var.domain
}

module "s3_policy" {
  source = "../../modules/s3-policy"
  domain = var.domain
  aws_s3_bucket_name = aws_s3_bucket.this.id
}

resource "aws_s3_bucket_website_configuration" "this" {
  bucket = aws_s3_bucket.this.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "404.html"
  }
}