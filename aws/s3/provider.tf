provider "aws" {
  region = var.region

  default_tags {
    tags = {
      creation = "terraform"
      repo = "tf-infrastructure"
    }
  }
}