# S3

AWS S3 provides object storage, as well as simple static site hosting.

## Required Variables 

| Variable            | Source                    | Category  | 
|-------------------- | ------------------------- | --------- |
| CLOUDFLARE_API_TOKEN | [Instructions](https://developers.cloudflare.com/fundamentals/api/get-started/create-token/) | env      |
| domain              | E.g. example.com          | terraform |

