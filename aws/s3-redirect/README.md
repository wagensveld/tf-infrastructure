# S3 Redirect

Very similar to the S3 definition, but exclusively for S3 redirects.

## Required Variables 

| Variable            | Source                    | Category  | 
|-------------------- | ------------------------- | --------- |
| CLOUDFLARE_API_TOKEN | [Instructions](https://developers.cloudflare.com/fundamentals/api/get-started/create-token/) | env      |
| domain              | E.g. example.com          | terraform |

