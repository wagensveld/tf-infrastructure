resource "cloudflare_record" "this" {
  zone_id = data.cloudflare_zone.this.id
  name    = var.subdomain
  type    = "CNAME"
  value   = aws_s3_bucket_website_configuration.this.website_domain
  proxied = true
  comment = var.comment
}