terraform {
  cloud {
    hostname = "app.terraform.io"
    organization = "wagensveld"
    workspaces {
      tags = ["s3-redirect"]
    }
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.18"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.15"
    }
  }
}
