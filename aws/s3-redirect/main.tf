resource "aws_s3_bucket" "this" {
  bucket = local.full_domain
}

module "s3_policy" {
  source = "../../modules/s3-policy"
  domain = local.full_domain
  aws_s3_bucket_name = aws_s3_bucket.this.id
}

resource "aws_s3_bucket_website_configuration" "this" {
  bucket = aws_s3_bucket.this.id

  redirect_all_requests_to {
    host_name = var.domain
    protocol = "https"
  }
}