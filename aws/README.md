# AWS

AWS provides many cloud services. For the sake of hosting a static website it's probably overkill.

## Access

1. [Create an IAM OIDC identity provider](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_create_oidc.html).

| Entry        | Value                    |
| ------------ | ------------------------ |
| Provider URL | https://app.terraform.io |
| Audience     | aws.workload.identity    |

2. Asign a role to the identity provider.

| Entry        | Value                    |
| ------------ | ------------------------ |
| Trusted entity type | Web Identity |
| Identity provider     | app.terraform.io    |
| Audience     | aws.workload.identity    |
| Permissions | Required permissions for what you want to deploy | 

3. Lock down the role's trust policy. Under the default `"StringEquals"` block add the following:

```json
"StringLike": {
  "app.terraform.io:sub": "organization:ORG_NAME:project:*:workspace:*:run_phase:*"
}
```
  At the minimum you should lock down the organisation name. Ideally projects and workspaces would be fine tuned.  

## Required Variables 

| Variable              | Source                    | Category | 
|---------------------- | ------------------------- | -------- |
| TFC_AWS_PROVIDER_AUTH | true                      | env      |
| TFC_AWS_RUN_ROLE_ARN  | IAM Role you have created | env      |
