resource "aws_s3_bucket_public_access_block" "this" {
  bucket                  = data.aws_s3_bucket.this.id
  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_policy" "this" {
  depends_on = [aws_s3_bucket_public_access_block.this]
  bucket     = data.aws_s3_bucket.this.id
  policy     = data.aws_iam_policy_document.s3_website.json
}
