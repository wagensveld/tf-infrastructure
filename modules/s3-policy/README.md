# S3 Policy

The S3 policies for a regular S3 bucket for static sites, and a redirection one are the same. It makes sense to wrap them up in a module.
