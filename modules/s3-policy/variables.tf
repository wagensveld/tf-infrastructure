variable "region" {
  type        = string
  description = "Default AWS region"
  default     = "ap-southeast-2"
}

variable "domain" {
  type        = string
  description = "Static site domain name"
}

variable "aws_s3_bucket_name" {
  type        = string
  description = "Name of the AWS S3 bucket"
}
