# TF Infrastructure

This monorepo contains the infrastructure for the following websites:

- [svw.au](https://svw.au)
- [hipster.melbourne](https://hipster.melbourne)

## Structure

Directories follow a `./provider/service` format. Both provider and service directories contain README.md's with more information.

## Set Up

### Terraform Cloud

1. Create a [Terraform Cloud](https://www.terraform.io/) account.

2. Create a project. The structure is up to you, I find it easiest to split projects by provider.

3. Create a workspace using *Version control workflow* as your workflow. This should be per service. (E.g. one for `cloudflare/email`, another for `cloudflare/zone`). 

    Under *Workspace Settings - General* set the working directory to the path of the service. (E.g. `cloudflare/email`).

    Under *Workspace Settings - Version Control* ensure *VCS Triggers* is set to *Only trigger runs when files in specified paths change* and that path is the path of the service.

#### Variables 

Some extra variables are needed, they're mentioned in the README.md's in the respective provider and service directories.

Most variables should be set per workspace, however frequently used variables can be configured as variable sets and applied to entire workspaces.